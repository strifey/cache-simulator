CXXFLAGS := -g -Wall -std=c++0x -lm
CXX=g++

SRC_DIR = ./src
OUT_DIR = ./bin
SRC_FILES = cachesim_driver.cpp cachesim.cpp line.cpp block.cpp cache.cpp 
O_FILES = $(addprefix $(OUT_DIR)/,$(SRC_FILES:.cpp=.o))
SRC_C_FILES = $(addprefix $(SRC_DIR)/,$(SRC_FILES))

build : $(O_FILES) 
		$(CXX) $(CXXFLAGS) $(O_FILES) -o ./cache_sim

remake : clean build

$(OUT_DIR)/%.o : $(SRC_DIR)/%.cpp
		$(CXX) -c $(CXXFLAGS) $< -o $@

clean :
	rm -f ./bin/*.o ./cache_sim
