===================
Astar
===================
Cache Settings
C: 15
B: 9
S: 2
F: EAGER
R: LRU

(AAT): 2.024719

===================
Bzip2
===================
Cache Settings
C: 15
B: 12
S: 2
F: EAGER
R: LRU

(AAT): 1.004870

===================
MCF
===================
Cache Settings
C: 15
B: 8
S: 2
F: EAGER
R: LRU

(AAT): 1.482001

===================
Perlbench
===================
Cache Settings
C: 15
B: 7
S: 2
F: BLOCKING
R: LRU

(AAT): 3.013495
