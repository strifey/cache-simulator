#include "block.h"

using std::cout;
using std::endl;

Block::Block(uint64_t _b, char _f, char _r)
	:b(_b), f(_f), r(_r){
		if(f == BLOCKING){
			valid.push_back(false);
		}
		else { //EAGER
			int valid_bits = (int)pow(2, b);
			for(int i = 0; i < valid_bits; i++)
				valid.push_back(false);
		}
		dirty = false;
}

bool Block::access(uint64_t b_off, char type){
	if(type == WRITE)
		dirty = true;

	if(f == BLOCKING){
		if(!valid.at(0)){
			valid.at(0) = true;
			return false;
		}
	}
	else{//EAGER
		if(!valid.at(b_off)){
			for(uint64_t i = b_off; i< valid.size(); i++)
				valid.at(i) = true;
			return false;
		}
	}
	return true;
}

void Block::populate(uint64_t _tag, uint64_t b_off, char type){
	tag = _tag;
	if(f == EAGER){
		for(uint64_t i = 0; i < b_off; i++)
			valid.at(i) = false;
		for(uint64_t i = b_off; i < valid.size(); i++)
			valid.at(i) = true;
	}
	//Blocking
	else valid.at(0) = true;

	if(type == WRITE)
		dirty = true;
	else //READ
		dirty = false;
}

Block::~Block(){
};
