#include <cstdint>
#include <iostream>
#include <cstdlib>
#include <vector>
#include <cmath>
#include "cachesim.h"

#ifndef BLOCK_H_
#define BLOCK_H_

class Block{
	private:
		uint64_t b;
		char f,r;
	public:
		Block(uint64_t _b, char f, char r);
		uint64_t tag;
		bool dirty;
		std::vector<bool> valid;
		bool access(uint64_t b_off, char type);
		void populate(uint64_t _tag, uint64_t b_off, char type);
		~Block();
};

#endif
