#include "cache.h"
/*
 * Sets up bit offsets and masks for tag, index, and block_off
 */

using std::cout;
using std::endl;
using std::hex;

Cache::Cache(uint64_t _c, uint64_t _s ,uint64_t _b, char _f, char _r)
:c(_c),s(_s),b(_b),f(_f),r(_r){
	//First calculate index values
	uint64_t index_size = c - (b+s);
	uint64_t index_rows = (uint64_t)pow(2, index_size);
	//Create each cache line(each block set)
	for(uint64_t i = 0; i < index_rows; i++)
		lines.push_back(Line(_s,_b, _f, _r));
	//Determine masks and offsets to make accessing easier
	block_mask = ((uint64_t)pow(2,b))-1;
	index_off = b;
	index_mask = ((uint64_t)pow(2,index_size))-1;
	tag_off = b+index_size;
}

//interpret address and update line & stats accordingly
void Cache::access(char type, uint64_t address, cache_stats_t* p_stats){
	//Update stats first
	p_stats->accesses++;
	if(type == READ)
		p_stats->reads++;
	else
		p_stats->writes++;
	//Calculate values for accessing the line
	uint64_t b_off = address & block_mask;
	uint64_t index = (address>>index_off) & index_mask;
	uint64_t tag = address>>tag_off;
	//Actually access the line
	lines.at(index).access(tag, b_off, type, p_stats);
}

//Hit time, misses, AAT, overhead ratio, etc.
void Cache::final_calculations(cache_stats_t* p_stats){
	//Total misses
	p_stats->misses = p_stats->read_misses + p_stats->write_misses;
	//Hit Time
	p_stats->hit_time =(uint64_t) ceil(.2*pow(2,s));
	//Miss Penalty
	if(f == BLOCKING)
		p_stats->miss_penalty =(uint64_t) 
			ceil(.25*pow(2,b)+50+p_stats->hit_time);
	else //if f == EAGER
		p_stats->miss_penalty = 51 + p_stats->hit_time;
	//Miss Rate
	p_stats->miss_rate = ((float)p_stats->misses)/p_stats->accesses;
	//AAT
	p_stats->avg_access_time = 
		p_stats->hit_time + (p_stats->miss_rate*p_stats->miss_penalty);
	//Overhead
	uint64_t dirty_oh, rep_oh, valid_oh = 0;
	dirty_oh = (uint64_t) pow(2, c-b);
	if(f == BLOCKING)
		rep_oh = dirty_oh; //just another bit, so same as dirty_oh
	else //if f == EAGER
		rep_oh = dirty_oh*((uint64_t)pow(2,b));
	if(r == LRU)
		valid_oh = 8*dirty_oh;
	else //if r == NMRU_FIFO
		valid_oh = 4*dirty_oh;
	uint64_t tag_oh = (64-(c-s))*dirty_oh;
	p_stats->storage_overhead = dirty_oh+rep_oh+valid_oh+tag_oh;
	p_stats->storage_overhead_ratio = 
		(((float)p_stats->storage_overhead)/8)/pow(2,c);
}
