#include <cstdint>
#include <cmath>
#include <iostream>
#include <vector>
#include <memory>
#include "cachesim.h"
#include "line.h"
#include "block.h"

#ifndef CACHE_H_
#define CACHE_H_

#define print_total (0)
/*
 * Definition of Cache class.
 * Holds all metadata about cache as well as each set index line.
 * Also holds masks and offsets to interpret addresses more easily.
 *
 * Has functions for accessing cache and updating statistics.
 * Updates statistics with static data at the end, since we don't get the 
 * stats pointer until then.
 */
class Cache{
	public:
		Cache(uint64_t _c, uint64_t _s ,uint64_t _b, char _f, char _r);
		void access(char type, uint64_t arg, cache_stats_t* p_stats);
		void final_calculations(cache_stats_t* p_stats);
	private:
		uint64_t c, s, b;
		uint64_t index_off, index_mask;
		uint64_t block_mask;
		uint64_t tag_off, tag_mask;
		char f,r;
		std::vector<Line> lines;
	protected:
};

#endif
