#include "line.h"

using std::cout;
using std::endl;

//Sets up basic values needed for each set line
Line::Line(char _s, char _b, char _f, char _r):
	s(_s), b(_b), f(_f), r(_r){
		//Initialize each block in the set
		uint64_t set_size = (uint64_t) pow(2, s);
		for(uint64_t i = 0; i<set_size; i++){
			blocks.push_back(Block(b, f, r));
			repl_list.push_back(i);
		}
		mru = 0;
	}

//Handles updating metadata for each access and calculating times for hits/misses
void Line::access(uint64_t tag, uint64_t b_off, 
		char type, cache_stats_t* p_stats){
	//Try to access the block first. and let it update it's values/stats
	unsigned int i = 0;
	bool valid,found = false;
	for(i = 0; i<blocks.size(); i++){
		if(blocks.at(i).tag == tag){
			valid = blocks.at(i).access(b_off, type);
			found = true;
			break;
		}
	}
	//Parse results of access attempt and update stats
	if(found){
		if(valid){
			if(type == WRITE)
				p_stats->write_hits++;
			else
				p_stats->read_hits++;
		}
		else{ //if !valid
			if(type == WRITE)
				p_stats->write_misses++;
			else
				p_stats->read_misses++;
		}
		if(r == LRU){
			repl_list.remove(i);
			repl_list.push_back(i);
		}
		else //if NMRU_FIFO
			mru = i;
	}
	//Handling not finding the block's tag in the cache
	else { // if !found
		//Record the miss
		if(type == WRITE)
			p_stats->write_misses++;
		else
			p_stats->read_misses++;
		//Handle eviction/replacement
		int evicted = 0;
		if(r == LRU)
			evicted = repl_list.front();
		else{ //if r == NMRU_FIFO
			if(mru != repl_list.front())
				evicted = repl_list.front();
			else {
				int front = repl_list.front();
				repl_list.pop_front();
				evicted = repl_list.front();
				repl_list.push_front(front);
			}
			mru = evicted;
		}
		repl_list.remove(evicted);
		blocks.at(evicted).populate(tag, b_off, type);
		repl_list.push_back(evicted);
	}
}
