#include <cstdint>
#include <cmath>
#include <vector>
#include <iostream>
#include <list>
#include "block.h"
#include "cachesim.h"

#ifndef LINE_H_
#define LINE_H_

/*
 * Line of blocks for each set in the cache.
 *
 * Holds relevant metadata to calculating stats at this level
 * Holds data for eviction determination
 * Holds blocks on this line of the cache
 */
class Line{
	private:
		uint64_t mru;
		char s, b;
		char f, r;
		std::vector<Block> blocks;
		std::list<uint64_t> repl_list;
	public:
		Line(char _s, char _b, char _f, char _r);
		void access(uint64_t tag, uint64_t block, char type, 
				cache_stats_t* p_stats);
};

#endif
